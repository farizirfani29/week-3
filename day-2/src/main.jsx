import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./base.module.scss";

// theme

ReactDOM.createRoot(document.getElementById("root")).render(<App />);
