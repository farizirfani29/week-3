import CountryList from "../components/FlagCountry";
import Navbar from "../components/Navbar";
import { CssBaseline } from "@mui/material";
import { useMemo, useState, useEffect } from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";

const MainCountry = () => {
  const [darkMode, setDarkMode] = useState(false);

  const theme = useMemo(() => {
    return createTheme({
      palette: {
        mode: darkMode ? "dark" : "light",
        primary: {
          main: darkMode ? "#fff" : "#000",
        },
        background: {
          default: darkMode ? "#2b3642" : "#fff",
        },
      },
      components: {
        MuiAppBar: {
          styleOverrides: {
            root: {
              backgroundColor: darkMode ? "#2b3642" : "#fff",
              color: darkMode ? "#fff" : "#000",
            },
          },
        },
        MuiCard: {
          styleOverrides: {
            root: {
              backgroundColor: darkMode ? "#2b3642" : "#fff",
            },
          },
        },
        menuPaper: {
          styleOverrides: {
            root: {
              backgroundColor: darkMode ? "#2b3642" : "#fff",
              '&[role="menu"]': {
                backgroundColor: "#2b3642",
              },
            },
          },
        },
      },
    });
  }, [darkMode]);

  // const toggleDarkMode = () => {
  //   setDarkMode((prevMode) => !prevMode);
  // };
  const toggleDarkMode = () => {
    setDarkMode((prevMode) => {
      localStorage.setItem("darkMode", !prevMode);
      return !prevMode;
    });
  };

  useEffect(() => {
    const savedDarkMode = localStorage.getItem("darkMode");
    if (savedDarkMode) {
      setDarkMode(savedDarkMode === "true");
    }
  }, []);

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="main-page">
          <Navbar darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
          <CountryList />
        </div>
      </ThemeProvider>
    </>
  );
};

export default MainCountry;
