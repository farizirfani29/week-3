import axios from "axios";
import Navbar from "../components/Navbar";
import scss from "../assets/style/DetailCountry.module.scss";
import { Link } from "react-router-dom";
import { useMemo, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { CssBaseline, Skeleton } from "@mui/material";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import { ThemeProvider, createTheme } from "@mui/material/styles";

const DetailCountry = () => {
  const [dataDetailCountry, setDataDetailCountry] = useState([]);
  const { countryName } = useParams();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchDetailCountry = async () => {
      try {
        await new Promise((resolve) => setTimeout(resolve, 1000));
        const response = await axios.get(
          `https://restcountries.com/v3.1/name/${countryName}`,
          setLoading(false)
        );
        setDataDetailCountry(response.data[0]);
      } catch (error) {
        console.error(error), setLoading(false);
      }
    };
    fetchDetailCountry();
  }, [countryName]);

  const [darkMode, setDarkMode] = useState(false);

  const theme = useMemo(() => {
    return createTheme({
      palette: {
        mode: darkMode ? "dark" : "light",
        primary: {
          main: darkMode ? "#000" : "#fff",
        },
        background: {
          default: darkMode ? "#2b3642" : "#fff",
          paper: darkMode ? "#2b3642" : "#fff",
        },
      },
      components: {
        MuiAppBar: {
          styleOverrides: {
            root: {
              backgroundColor: darkMode ? "#2b3642" : "#fff",
              color: darkMode ? "#fff" : "#000",
            },
          },
        },
        MuiButton: {
          styleOverrides: {
            root: {
              background: darkMode ? "#2b3642" : "#fff",
            },
          },
        },
      },
    });
  }, [darkMode]);
  const toggleDarkMode = () => {
    setDarkMode((prevMode) => {
      localStorage.setItem("darkMode", !prevMode);
      return !prevMode;
    });
  };
  useEffect(() => {
    const savedDarkMode = localStorage.getItem("darkMode");
    if (savedDarkMode) {
      setDarkMode(savedDarkMode === "true");
    }
  }, []);

  const nativeName = dataDetailCountry.name?.nativeName
    ? Object.values(dataDetailCountry.name?.nativeName)
    : [];

  const currencies = dataDetailCountry.currencies
    ? Object.keys(dataDetailCountry.currencies)
    : "-";

  const languages = dataDetailCountry.languages
    ? Object.keys(dataDetailCountry?.languages)
    : [];

  const borders = dataDetailCountry.borders ? dataDetailCountry.borders : [];

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Navbar darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
        <div style={{ width: "200px" }}>
          <Link to="/" width="100%" style={{ textDecoration: "none" }}>
            <div className={scss.backBtn}>
              <Button
                variant="contained"
                className={scss.btn}
                sx={{
                  boxShadow: 4,
                }}
              >
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M20.3284 11.0001V13.0001L7.50011 13.0001L10.7426 16.2426L9.32842 17.6568L3.67157 12L9.32842 6.34314L10.7426 7.75735L7.49988 11.0001L20.3284 11.0001Z"
                    fill="currentColor"
                  />
                </svg>
                Back
              </Button>
            </div>
          </Link>
        </div>
        {loading ? (
          <Box
            display="flex"
            flexWrap={"wrap"}
            alignItems="center"
            // justifyContent="center"
            padding={"50px"}
          >
            <Skeleton variant="rounded" width={500} height={300} rounded={8} />
            <Box
              display="flex"
              justifyContent="center"
              height={"100%"}
              width={600}
              gap={2}
              flexWrap={"wrap"}
            >
              <Box
                width={250}
                height={"100%"}
                display={"flex"}
                flexDirection={"column"}
                gap={3}
                flexWrap={"wrap"}
              >
                <Skeleton height={"35px"} variant="text" width="100%" />
                <Skeleton height={"35px"} variant="text" width="100%" />
                <Skeleton height={"35px"} variant="text" width="100%" />
                <Skeleton height={"35px"} variant="text" width="100%" />
              </Box>
              <Box
                width={250}
                height={"100%"}
                display={"flex"}
                flexDirection={"column"}
                gap={3}
              >
                <Skeleton height={"35px"} variant="text" width="100%" />
                <Skeleton height={"35px"} variant="text" width="100%" />
                <Skeleton height={"35px"} variant="text" width="100%" />
                <Skeleton height={"35px"} variant="text" width="100%" />
              </Box>
            </Box>
          </Box>
        ) : (
          <div className={scss.container_detailCountry}>
            <div className="box_detail_img">
              <img
                className={scss.imgDetailCountry}
                src={dataDetailCountry.flags?.png}
                alt={dataDetailCountry.name?.common}
              />
            </div>
            <div className={scss.container_text}>
              <h2 className={scss.title_country}>
                {dataDetailCountry.name?.common || "-"}
              </h2>

              <div className={scss.descCountry}>
                <div className={scss.box_detail_text_1}>
                  <p>Native Name: {nativeName[0]?.common || "-"}</p>
                  <p>
                    Population :{" "}
                    {dataDetailCountry &&
                      dataDetailCountry.population &&
                      dataDetailCountry.population.toLocaleString("id")}
                  </p>
                  <p>Region : {dataDetailCountry.region || "-"}</p>
                  <p>Sub Region : {dataDetailCountry.subregion || "-"}</p>
                  <p>Capital: {dataDetailCountry.capital || "-"}</p>
                </div>
                <div className={scss.box_detail_text_2}>
                  <p>Top Level Domain: {dataDetailCountry.tld}</p>
                  <p>Currencies: {currencies}</p>
                  {languages.length > 0 && (
                    <div className={scss.language}>
                      <p>Languages: </p>
                      {languages.map((language) => (
                        <Box key={language}>
                          <span className={scss.span_languange}>
                            {" " + dataDetailCountry.languages[language] + ","}
                          </span>
                        </Box>
                      ))}
                    </div>
                  )}
                </div>
              </div>
              <div className={scss.box_border}>
                {borders.length > 0 && (
                  <div className={scss.box_border}>
                    <p>Border Countries:</p>
                    {borders.map((border) => (
                      <Box
                        key={border}
                        sx={{
                          boxShadow: 3,
                        }}
                      >
                        <div className={scss.border}>{border}</div>
                      </Box>
                    ))}
                  </div>
                )}
              </div>
            </div>
          </div>
        )}
      </ThemeProvider>
    </>
  );
};

export default DetailCountry;
