import { useEffect, useState } from "react";
import axios from "axios";
import scss from "../assets/style/FlagCountry.module.scss";
import { Link } from "react-router-dom";
import SortByAlphaIcon from "@mui/icons-material/SortByAlpha";

import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Skeleton,
  Pagination,
} from "@mui/material";

const CountryList = () => {
  const [countriesData, setCountriesData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedRegion, setSelectedRegion] = useState("");
  const [searchKeyword, setSearchKeyword] = useState("");
  const [sortOrder, setSortOrder] = useState("ascending");
  const [page, setPage] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await new Promise((resolve) => setTimeout(resolve, 1000));
        const response = await axios.get("https://restcountries.com/v3.1/all");
        setCountriesData(response.data);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching data:", error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const handlePageChange = (event, value) => {
    setPage(value);
  };

  const handleRegionChange = (event) => {
    setSelectedRegion(event.target.value);
    setSearchKeyword("");
  };

  const handleSearchChange = (event) => {
    setSearchKeyword(event.target.value);
  };

  const filteredCountries = countriesData.filter((country) => {
    const regionMatch =
      selectedRegion === "" || country.region === selectedRegion;
    const keywordMatch =
      searchKeyword === "" ||
      country.name.common.toLowerCase().includes(searchKeyword.toLowerCase());
    return regionMatch && keywordMatch;
  });

  const sortCountries = () => {
    setCountriesData((prevCountriesData) =>
      prevCountriesData.slice().sort((a, b) => {
        const nameA = a.name.common.toLowerCase();
        const nameB = b.name.common.toLowerCase();
        if (sortOrder === "ascending") {
          return nameA.localeCompare(nameB);
        } else {
          return nameB.localeCompare(nameA);
        }
      })
    );

    // Toggle the sortOrder after sorting
    setSortOrder((prevSortOrder) =>
      prevSortOrder === "ascending" ? "descending" : "ascending"
    );
  };

  const itemsPerPage = 10;
  const totalPages = Math.ceil(filteredCountries.length / itemsPerPage);
  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = Math.min(
    startIndex + itemsPerPage,
    filteredCountries.length
  );

  const currentCountries = filteredCountries.slice(startIndex, endIndex);

  return (
    <div className={scss.container_utils}>
      <div className={scss.util_box}>
        <TextField
          className={scss.input_search}
          label="Search Country"
          value={searchKeyword}
          onChange={handleSearchChange}
        />
        <div className={scss.box_filter_asc}>
          <FormControl fullWidth>
            <InputLabel>Filter by Region</InputLabel>
            <Select
              className={scss.selectFilter}
              value={selectedRegion}
              onChange={handleRegionChange}
              label="Filter by Region"
            >
              <MenuItem value="">Filter by Region</MenuItem>
              <MenuItem value="Africa">Africa</MenuItem>
              <MenuItem value="Americas">Americas</MenuItem>
              <MenuItem value="Asia">Asia</MenuItem>
              <MenuItem value="Europe">Europe</MenuItem>
              <MenuItem value="Oceania">Oceania</MenuItem>
            </Select>
          </FormControl>
          <Button
            sx={{
              boxShadow: 4,
            }}
            onClick={sortCountries}
          >
            <SortByAlphaIcon />
          </Button>
        </div>
      </div>

      <div className={scss.container_card}>
        {loading ? (
          <>
            {[...Array(10)].map((_, index) => (
              <div key={index} className={scss.Card_contry}>
                <Card sx={{ maxWidth: 250 }}>
                  <Skeleton variant="text" width={250} height={180} />
                  <CardActions>
                    <Skeleton variant="text" width="50%" />
                  </CardActions>
                  <CardContent>
                    <Skeleton variant="text" width={210} height={20} />
                    <Skeleton variant="text" width={210} height={20} />
                    <Skeleton variant="text" width={210} height={20} />
                    <Skeleton variant="text" width={210} height={20} />
                  </CardContent>
                </Card>
              </div>
            ))}
          </>
        ) : (
          <>
            {currentCountries.map((country, index) => (
              <div key={index} className={scss.Card_contry}>
                <Card sx={{ maxWidth: 250 }}>
                  <Link to={`/countries/${country.name.common}`}>
                    <CardMedia>
                      {country.flags && (
                        <img
                          src={country.flags.png}
                          alt={country.name.common}
                          width="100%"
                          height={180}
                        />
                      )}
                    </CardMedia>
                  </Link>
                  <CardContent>
                    <h4>{country.name.common || "-"}</h4>
                    <p>
                      Population :{" "}
                      {country.population.toLocaleString("id") || "-"}
                    </p>
                    <p>Region : {country.region || "-"}</p>
                    <p>Capital : {country.capital || "-"}</p>
                  </CardContent>
                </Card>
              </div>
            ))}
          </>
        )}
      </div>
      <Pagination
        display={"flex"}
        justifycontent={"center"}
        count={totalPages}
        page={page}
        onChange={handlePageChange}
        color="primary"
        size="large"
        className={scss.pagination}
      />
    </div>
  );
};

export default CountryList;
