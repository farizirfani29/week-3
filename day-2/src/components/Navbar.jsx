import PropTypes from "prop-types";
import scss from "../assets/style/Navbar.module.scss";
import { AppBar, Toolbar, Typography, IconButton } from "@mui/material";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

const Navbar = ({ darkMode, toggleDarkMode }) => {
  return (
    <AppBar position="sticky">
      <Toolbar className={scss.Navbar}>
        <Typography
          variant="h5"
          component="div"
          className={scss.logo}
          sx={{ flexGrow: 1, fontWeight: "bold" }}
        >
          Where in the world?
        </Typography>

        <IconButton onClick={toggleDarkMode} color="inherit">
          {darkMode ? <Brightness7Icon /> : <Brightness4Icon />}
        </IconButton>

        <Typography
          inputprops={{ "aria-label": "toggle theme" }}
          color="default"
          checked={darkMode}
          onClick={toggleDarkMode}
        >
          Dark mode
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

Navbar.propTypes = {
  darkMode: PropTypes.bool,
  toggleDarkMode: PropTypes.func,
};

export default Navbar;
