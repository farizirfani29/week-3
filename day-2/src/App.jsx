import DetailCountry from "./pages/detailCountry";
import MainContry from "./pages/mainContry";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainContry />} />
          <Route path="/countries/:countryName" element={<DetailCountry />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
