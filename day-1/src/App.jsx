import "./style/App.css";
import Timer from "./components/Timer.jsx";

function App() {
  return (
    <>
      <div className="container">
        <Timer />
      </div>
    </>
  );
}

export default App;
