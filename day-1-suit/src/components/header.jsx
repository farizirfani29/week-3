import '../style/Header.scss'


const Header = () => {
  return (
      <>
          <div className="Header">
              <div className="content">
                  <h1>ROCK <br /> PAPER <br />  SCISSORS</h1>
                  <div className="score-box">
                      <h1 className='score-text'>score</h1>
                      <h1 className='score'>12</h1>
                  </div>
              </div>
          </div>
      </>
  )
}

export default Header