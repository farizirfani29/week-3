import './style/App.scss'
import Header from './components/header'
import Content from './components/content'

function App() {
  return (
    <>
      <div className='main'>
        <div className="container">
        <Header />
        <Content />
        </div>
      </div>
    </>
  )
}

export default App
